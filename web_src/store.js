import Vue from "vue";
import Vuex from "vuex";
import moment from "moment";

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        serverInfo: {},
        userInfo: null,
        menus: [
            {
                id: "menu_homepage",
                path: '/',
                title: "概览",
                icon: 'dashboard',
                roles: ['超级管理员'],
            }, {
                path: "/index/icons",
                // path: "/index/icons?online_dropdown=yes",
                icon: "video-camera",
                title: "视频广场",
                // roles: ['管理员','观众'],
            }, {
                id:"menu_homepage",
                path: '/screen',
                title: "分屏展示",
                icon: 'th-large',
                // roles: ['管理员','观众'],
            }, {
                path: "/record",
                icon: "backward",
                title: "录像回看",
                versionType: "录像版",
                // roles: ['管理员','观众'],
            }, {
                path: "/plan",
                icon: "calendar",
                title: "录像计划",
                versionType: "录像版",
                roles: ['管理员'],
            }, {
                path: "/channel",
                icon: "road",
                title: "通道配置",
                roles: ['管理员'],
            }, {
                path: "/user",
                icon: "users",
                title: "用户管理",
                roles: ['管理员'],
            }, {
                path: "/logs",
                icon: "file",
                title: "操作日志",
                roles: ['超级管理员'],
            }, {
                path: "/config",
                icon: "cogs",
                title: "基础配置",
                roles: ['超级管理员'],
            }, {
                path: "/about",
                icon: "support",
                title: "版本信息",
                roles: ['超级管理员'],
            }
        ]
    },
    mutations: {
        updateUserInfo(state, userInfo) {
            state.userInfo = userInfo;
        },
        updateServerInfo(state, serverInfo){
            state.serverInfo = serverInfo;
        }
    },
    actions : {
        getUserInfo({ commit, state }, payload) {
            return new Promise((resolve, reject) => {
                $.get("api/v1/getuserinfo", payload).then(msg => {
                    try {
                        if((msg.LiveQing || msg.NVR).Header.ErrorNum == "200") {
                            var userInfo = (msg.LiveQing || msg.NVR).Body.UserInfo;
                            commit('updateUserInfo', userInfo);
                            resolve(userInfo);
                            return;
                        }
                    } catch (error) {
                       console.log(error);
                    }
                    resolve(null);
                }).fail(() => {
                    resolve(null);
                })
            })
        },
        logout({ commit, state }) {
            return new Promise((resolve, reject) => {
                $.get('api/v1/logout').then(data => {
                    commit('updateUserInfo', null);
                    resolve(null);
                }).fail(() => {
                    resolve(null);
                })
            })
        },
        getServerInfo({commit}){
            return new Promise((resolve, reject) => {
                $.ajax({
                    method: "GET",
                    global: false,
                    url: "api/v1/getserverinfo",
                }).then(ret => {
                    try {
                        var serverInfo = Object.assign({}, (ret.LiveQing || ret.NVR).Body, (ret.LiveQing || ret.NVR).Header);
                        if (serverInfo.ServerTime) {
                            var stime = moment(serverInfo.ServerTime, "YYYY-MM-DD HH:mm:ss");
                            serverInfo.DiffDuration = moment.duration(stime.diff(moment()));
                        }
                        commit('updateServerInfo', serverInfo);
                        resolve(serverInfo);
                        return
                    } catch (error) {
                        console.log(error);
                    }
                    resolve({});
                }).fail(() => {
                    resolve({});
                })
            })
        }
    }
})

export default store;